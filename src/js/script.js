document.addEventListener("DOMContentLoaded", function(event) {

    //connexion
    document.getElementById("connexion-lien").addEventListener("click", function (event) {
        event.preventDefault();
        document.querySelector("#connexion-modale").classList.add("open");
    });

    document.getElementById("fermer-connexion-modale").addEventListener("click", function (event) {
        event.preventDefault();
        document.querySelector("#connexion-modale").classList.remove("open");
    });

    //arrow
    document.getElementById("arrow-up").addEventListener("click", function(event){
        event.preventDefault();

        var intervalid = 0;

        function scrollStep() {
            if (window.pageYOffset === 0){
                clearInterval(intervalid)
            }
            window.scroll(0, window.pageYOffset - 50);
        }

        function scrollToTop() {
            intervalid = setInterval(scrollStep, 16.66);
        }

        window.scrollTo({
            top: 0,
            behavior: 'smooth'
        });
    })

});